CREATE TABLE `companydetails` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comapnyId` varchar(100) NOT NULL,
  `companyName` varchar(200) DEFAULT NULL,
  `companyAddress` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;
