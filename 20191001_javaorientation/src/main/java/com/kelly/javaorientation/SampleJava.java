package com.kelly.javaorientation;

public class SampleJava {

	public String functionWithOutArg() {
		String returnstring= " Finction without arguments started";
		return returnstring;
	}
	
	public String firstFunction(String activity) {
		String returnstring= activity+" started";
		return returnstring;
	}
	
	public String secondFunction(String activity) {
		String returnstring= activity+" started";
		return returnstring;
	}

}
